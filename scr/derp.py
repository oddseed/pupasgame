from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.animation import Animation as A
from kivy.graphics import Color,Ellipse

class MainScreen():
	title_frame = BoxLayout(orientation='vertical',padding=[20,20,20,20])
	title = Label(text='PupusaGame',font_size=50,size_hint=(1,0.2))
	layout = GridLayout(cols=2,padding=[20,20,20,20])
	b1 = Button(text='Configuracion',font_size=50,size_hint=(0.5,0.4))
	b2 = Button(text='Puntaje',font_size=50,size_hint=(0.5,0.4))
	b3 = Button(text='Reto',font_size=50,size_hint=(0.5,0.4))
	b4 = Button(text='Historia',font_size=50,size_hint=(0.5,0.4))
	pupusa = Ellipse(pos=(100,100),size=(200,200))

	def __init__(self):
		self.title_frame.add_widget(self.title)
		self.layout.add_widget(self.b1,1)
		self.layout.add_widget(self.b2,2)
		self.layout.add_widget(self.b3,3)
		self.layout.add_widget(self.b4,4)
		self.title_frame.add_widget(self.layout)

class Screen(App):
	def build(self):
		s = MainScreen()
		return s.title_frame

class StoryMode(App):
    def build(self):
    	title_frame = BoxLayout(orientation='vertical',padding=[20,20,20,20])
    	title = Label(text='Modo Historia',font_size=50,size_hint=(1,0.2))
    	title_frame.add_widget(title)
    	frame = BoxLayout(orientation='horizontal')
    	frame.add_widget(Button(text='Nuevo',font_size=50,size_hint=(0.5,1.)),1)
    	frame.add_widget(Button(text='Cargar',font_size=50,size_hint=(0.5,1.)),2)
    	title_frame.add_widget(frame)
    	footer_frame = BoxLayout(orientation='horizontal',padding=[20,20,20,20])
    	footer = Label(text='Atras',font_size=20,size_hint=(1,0.05))
    	footer_frame.add_widget(footer)
    	title_frame.add_widget(footer_frame)
    	return title_frame

Screen().run()