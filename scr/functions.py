from gamesettings import PUP_TYPES
from random import randrange
 
def GetRandomType(l=3,limType=True):
	if limType:
		n = randrange(l)
		return PUP_TYPES[n]
	else:
		n = randrange(len(PUP_TYPES))
		return PUP_TYPES[n]

def GeneratePedido(max_pup=5,max_var=4,lim=3):
	"""
	max_pup : cantidad maxima de pupusas que puede pedir
	max_var : cantidad maxima de pupusas diferentes que puede pedir
	lim 	: limite ordenado en la progresion del tipo de pupusas que pueden pedir
	"""
	pedido = {}
	if lim == 0:
		pedido[GetRandomType(limType=False)]=randrange(1,max_pup)
	elif max_var == 1:
		pedido[GetRandomType(lim)]=randrange(1,max_pup)
	else:
		var = randrange(1,max_var)
		for x in range(var):
			pedido[GetRandomType(lim)]=randrange(1,max_pup)
	return pedido
