from kivy.app import App
from kivy.uix.widget import Widget
from logic_layer import *
from kivy.clock import Clock
from kivy.graphics import Color, Ellipse,Line,Rectangle
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.core.image import Image
from kivy.animation import Animation
from kivy.properties import BooleanProperty,StringProperty,NumericProperty,ObjectProperty
from kivy.config import Config
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from os import getcwd,path
from sound_layer import *
from kivy.core.window import Window
from random import getrandbits

PAUSE = False


class ChildBoxLayout(BoxLayout):
	posR = BooleanProperty(False)
	realsize = (100,100)
	realpos = (0,0)
	def __init__(self,**kwargs):
		super(ChildBoxLayout, self).__init__(**kwargs)

class ChildFloatLayout(FloatLayout):
	posR = BooleanProperty(False)
	realsize = (100,100)
	realpos = (0,0)
	def __init__(self,**kwargs):
		super(FloatLayout, self).__init__(**kwargs)


class ChildWidget(Widget):
	imagedir = StringProperty(path.join(getcwd()+'/images/'))
	img = StringProperty()
	posR = BooleanProperty(False)
	realsize = (100,100)
	realpos = (0,0)

class ImgButton(Button):
	img = StringProperty('back.png')
	imagedir = StringProperty(path.join(getcwd()+'/images/'))
	posR = BooleanProperty(False)
	realsize = (100,100)
	realpos = (0,0)
	still = BooleanProperty(True)

	def __init__(self,**kwargs):
		super(Button, self).__init__(**kwargs)
		self.background_color = [1,1,1,0]
		self.call = None

	def transition(self,callback):
		if self.still:
			self.a = Animation(size=(self.realsize[0]+10,self.realsize[1]+10), duration=0.25)+Animation(size=(self.realsize[0],self.realsize[1]), duration=0.25)
			self.a.bind(on_complete=self.animation_complete,on_progress=self.animation_prog)
			self.a.start(self)
	def animation_complete(self,a,b):
		self.call()

	def animation_prog(self,a,b,c):
		self.still = False

class PlanchaBG(ChildWidget):
	img = StringProperty('plancha.png')

class ScreenWidget(Widget):
	imagedir = StringProperty(path.join(getcwd()+'/images/'))

	def reposition_children(self, children):
		for child in children:
			if child.posR:
				child.pos = self.size[0]*(child.realpos[0]/600.),self.size[1]*(child.realpos[1]/300.)
			else:
				child.pos = self.size[0]*(child.realpos[0]/600.) + self.pos[0],self.size[1]*(child.realpos[1]/300.) + self.pos[1]
			child.size_hint = (self.size[0]/600.,self.size[1]/300.)
			child.size = (child.size_hint[0]*child.realsize[0],child.size_hint[1]*child.realsize[1])

class ButtonCancel(ChildWidget):
	img = StringProperty('btn_decline.png')

class ButtonAcept(ChildWidget):
	img = StringProperty('btn_accept.png')

class ChildGridLayout(GridLayout):
	posR = BooleanProperty(False)
	realsize = (100,100)
	realpos = (0,0)
	def __init__(self,**kwargs):
		super(ChildGridLayout, self).__init__(**kwargs)


class ScreenWidget(Widget):
	imagedir = StringProperty(path.join(getcwd()+'/images/'))

	def reposition_children(self, children):
		for child in children:
			if child.posR:
				child.pos = self.size[0]*(child.realpos[0]/600.),self.size[1]*(child.realpos[1]/300.)
			else:
				child.pos = self.size[0]*(child.realpos[0]/600.) + self.pos[0],self.size[1]*(child.realpos[1]/300.) + self.pos[1]
			child.size_hint = (self.size[0]/600.,self.size[1]/300.)
			child.size = (child.size_hint[0]*child.realsize[0],child.size_hint[1]*child.realsize[1])

class GameScreen(ScreenWidget):
	img = StringProperty('bg.png')
	screenup = BooleanProperty(True)
	showingh = BooleanProperty(False)
	skyn = 0
	r = NumericProperty(SKY_COLORS[skyn][0])
	g = NumericProperty(SKY_COLORS[skyn][1])
	b = NumericProperty(SKY_COLORS[skyn][2])

	def __init__(self,max_pup=5,max_var=4,lim=7,money=0.0,materials={'queso':100,'frijol':100,'loroco':100,'chich':100,'camaron':100},ladrones=False,Prisa=False, **kwargs):
		super(GameScreen, self).__init__(**kwargs)
		### LOGIC INTEGRATION ###
		self.DEFAULT_COMAL = Comal(16)
		self.p = None

		self.PUP_INV = []
		self.MONEY = money
		self.MAT = materials
		self.max_pup = max_pup
		self.max_var = max_var
		self.lim = lim
		self.money = money
		self.ladrones = ladrones
		self.Prisa = Prisa
		self.slide = False
		self.selected_client = None

		### LOGIC INTEGRATION ###
		
		self.clientarray = ArrayOfClients()
		self.clientarray.c1 = None
		self.clientarray.c2 = None
		self.clientarray.c3 = None
		self.clientarray.c4 = None

		self.plancha = PlanchaBG()
		self.hand_cntl = HandCntrl()
		self.queso = Ingredient()
		self.frijol = Ingredient()
		self.camaron = Ingredient()
		self.chich = Ingredient()
		self.loroco = Ingredient()
		self.masa = Ingredient()
		self.garbage = Garbage()
		self.hands = Hands()
		self.slot1= PupusaSlot()
		self.slot2= PupusaSlot()
		self.slot3= PupusaSlot()
		self.slot4= PupusaSlot()
		self.slot5= PupusaSlot()
		self.slot6= PupusaSlot()
		self.slot7= PupusaSlot()
		self.slot8= PupusaSlot()
		self.slot9= PupusaSlot()
		self.slot10= PupusaSlot()
		self.slot11= PupusaSlot()
		self.slot12= PupusaSlot()
		self.slot13= PupusaSlot()
		self.slot14= PupusaSlot()
		self.slot15= PupusaSlot()
		self.slot16= PupusaSlot()
		self.bar = PupaBar()
		self.accept = ButtonAcept()
		self.cancel = ButtonCancel()


		self.bar.realsize = (560,70)
		self.bar.realpos = (20,230)
		if DEBUG:
			self.dbg = DebugWindow(orientation='horizontal')
			self.dbg.posR = True
			self.dbg.realpos = (0,0)

		self.slot1.realpos = (132,-42)
		self.slot2.realpos = (180,-42)
		self.slot3.realpos = (228,-42)
		self.slot4.realpos = (276,-42)
		self.slot5.realpos = (324,-42)
		self.slot6.realpos = (372,-42)
		self.slot7.realpos = (420,-42)
		self.slot8.realpos = (90,-81)
		self.slot9.realpos = (138,-81)
		self.slot10.realpos = (186,-81)
		self.slot11.realpos = (234,-81)
		self.slot12.realpos = (282,-81)
		self.slot13.realpos = (330,-81)
		self.slot14.realpos = (378,-81)
		self.slot15.realpos = (426,-81)
		self.slot16.realpos = (474,-81)
		self.garbage.realpos = (396,-282)
		self.queso.realpos = (30,-150)
		self.frijol.realpos = (120,-150)
		self.camaron.realpos = (207,-150)
		self.chich.realpos = (294,-150)
		self.loroco.realpos = (384,-150)
		self.masa.realpos = (474,-150)
		self.hands.realpos = (240,-600)
		self.plancha.realpos = (0,0-157)
		self.plancha.realsize = (600,184)
		self.add_widget(self.clientarray)
		self.clientarray.realsize = (600,200)
		self.clientarray.realpos = (0,0)
		self.add_widget(self.plancha)
		
		self.add_widget(self.bar)
		self.add_widget(self.accept)
		self.add_widget(self.cancel)
		self.accept.realpos = (500,225)
		self.cancel.realpos = (550,225)
		self.accept.realsize = (25,25)
		self.cancel.realsize = (25,25)
		self.masa.ing = 'masa'
		self.queso.ing = 'queso'
		self.frijol.ing = 'frijol'
		self.camaron.ing = 'camaron'
		self.chich.ing = 'chich'
		self.loroco.ing = 'loroco'
		if 'queso' in self.MAT:
			self.add_widget(self.queso)
		if 'frijol' in self.MAT:
			self.add_widget(self.frijol)
		if 'camaron' in self.MAT:
			self.add_widget(self.camaron)
		if 'chich' in self.MAT:
			self.add_widget(self.chich)
		if 'loroco' in self.MAT:	
			self.add_widget(self.loroco)
		self.add_widget(self.masa)
		self.add_widget(self.garbage)
		self.add_widget(self.hands)
		self.add_widget(self.slot1)
		self.add_widget(self.slot2)
		self.add_widget(self.slot3)
		self.add_widget(self.slot4)
		self.add_widget(self.slot5)
		self.add_widget(self.slot6)
		self.add_widget(self.slot7)
		self.add_widget(self.slot8)
		self.add_widget(self.slot9)
		self.add_widget(self.slot10)
		self.add_widget(self.slot11)
		self.add_widget(self.slot12)
		self.add_widget(self.slot13)
		self.add_widget(self.slot14)
		self.add_widget(self.slot15)
		self.add_widget(self.slot16)

		if DEBUG:
			self.add_widget(self.dbg)
			self.dbg.realsize = (600,50)
		self.hand_cntl.realsize = (70,70)
		self.garbage.realsize = (75,100)
		self.hands.realsize = (150,175)
		self.PupusaBarDraw()



		self.masa.img = 'gm1.png'
		if 'chich' in self.MAT:
			self.chich.img = 'gc1.png'
		if 'camaron' in self.MAT:
			self.camaron.img = 'gca1.png'
		if 'loroco' in self.MAT:
			self.loroco.img = 'gl1.png'
		if 'queso' in self.MAT:
			self.queso.img = 'gq1.png'
		if 'frijol' in self.MAT:
			self.frijol.img = 'gf1.png'
		self.t = Timer()
		self.add_widget(self.t)
		self.t.Start()
		if DEBUG:
			self.redraw()

	def finished_sliding(self,a,b):
		self.slide = False

	def returnPupasof(self,p='pq'):
		l = []
		for n in self.PUP_INV:
			if n.getType() == p:
				l.append(n)
		return l

	def ClearSelected(self):
		self.pupas_queso.selected = 0
		self.pupas_chich.selected = 0
		self.pupas_revuelta.selected = 0
		self.pupas_camaron.selected = 0
		self.pupas_loroco.selected = 0
		self.pupas_fq.selected = 0
		self.pupas_frijol.selected = 0
		self.RefreshBar()

	def UpdateAmmounts(self):
		self.pupas_queso.ammount = len(self.returnPupasof('pq'))
		self.pupas_chich.ammount = len(self.returnPupasof('pch'))
		self.pupas_revuelta.ammount = len(self.returnPupasof('pr'))
		self.pupas_camaron.ammount = len(self.returnPupasof('pc'))
		self.pupas_loroco.ammount = len(self.returnPupasof('pl'))
		self.pupas_fq.ammount = len(self.returnPupasof('pfq'))
		self.pupas_frijol.ammount = len(self.returnPupasof('pf'))
		self.RefreshBar()

	def returnAllbut(self,p='pq'):
		l = []
		for n in self.PUP_INV:
			if not(n.getType() == p):
				l.append(n)
		return l

	def PupusaBarDraw(self):
		self.pupas_queso = PupaBar_Button(0)
		self.pupas_frijol = PupaBar_Button(1)
		self.pupas_fq = PupaBar_Button(2)
		self.pupas_loroco = PupaBar_Button(3)
		self.pupas_camaron = PupaBar_Button(4)
		self.pupas_chich = PupaBar_Button(5)
		self.pupas_revuelta = PupaBar_Button(6)
		self.add_widget(self.pupas_queso)
		self.add_widget(self.pupas_frijol)
		self.add_widget(self.pupas_fq)
		self.add_widget(self.pupas_loroco)
		self.add_widget(self.pupas_camaron)
		self.add_widget(self.pupas_chich)
		self.add_widget(self.pupas_revuelta)
		self.pupas_queso.realsize = (60,60)
		self.pupas_frijol.realsize = (60,60)
		self.pupas_fq.realsize = (60,60)
		self.pupas_loroco.realsize = (60,60)
		self.pupas_camaron.realsize = (60,60)
		self.pupas_chich.realsize = (60,60)
		self.pupas_revuelta.realsize = (60,60)
		self.pupas_queso.realpos = (25,240)
		self.pupas_frijol.realpos = (95,240)
		self.pupas_fq.realpos = (165,240)
		self.pupas_loroco.realpos = (235,240)
		self.pupas_camaron.realpos = (305,240)
		self.pupas_chich.realpos = (375,240)
		self.pupas_revuelta.realpos = (445,240)
		self.pp1 = Label(text='[color=#000000]'+str(self.pupas_queso.ammount)+'[/color]',markup = True)
		self.pp1.posR = False
		self.pp1.realpos = (55,270)
		self.pp1.realsize = (0,0)
		self.add_widget(self.pp1)
		self.pp2 = Label(text='[color=#000000]'+str(self.pupas_frijol.ammount)+'[/color]',markup = True)
		self.pp2.posR = False
		self.pp2.realpos = (125,270)
		self.pp2.realsize = (0,0)
		self.add_widget(self.pp2)
		self.pp3 = Label(text='[color=#000000]'+str(self.pupas_fq.ammount)+'[/color]',markup = True)
		self.pp3.posR = False
		self.pp3.realpos = (195,270)
		self.pp3.realsize = (0,0)
		self.add_widget(self.pp3)
		self.pp4 = Label(text='[color=#000000]'+str(self.pupas_loroco.ammount)+'[/color]',markup = True)
		self.pp4.posR = False
		self.pp4.realpos = (265,270)
		self.pp4.realsize = (0,0)
		self.add_widget(self.pp4)
		self.pp5 = Label(text='[color=#000000]'+str(self.pupas_camaron.ammount)+'[/color]',markup = True)
		self.pp5.posR = False
		self.pp5.realpos = (335,270)
		self.pp5.realsize = (0,0)
		self.add_widget(self.pp5)
		self.pp6 = Label(text='[color=#000000]'+str(self.pupas_chich.ammount)+'[/color]',markup = True)
		self.pp6.posR = False
		self.pp6.realpos = (405,270)
		self.pp6.realsize = (0,0)
		self.add_widget(self.pp6)
		self.pp7 = Label(text='[color=#000000]'+str(self.pupas_revuelta.ammount)+'[/color]',markup = True)
		self.pp7.posR = False
		self.pp7.realpos = (475,270)
		self.pp7.realsize = (0,0)
		self.add_widget(self.pp7)
		self.mm = Label(text='[color=#000000]$0.0[/color]',markup = True)
		self.p1 = Label(text='[color=#000000]'+str(self.pupas_queso.selected)+'[/color]',markup = True)
		self.p2 = Label(text='[color=#000000]'+str(self.pupas_frijol.selected)+'[/color]',markup = True)
		self.p3 = Label(text='[color=#000000]'+str(self.pupas_fq.selected)+'[/color]',markup = True)
		self.p4 = Label(text='[color=#000000]'+str(self.pupas_loroco.selected)+'[/color]',markup = True)
		self.p5 = Label(text='[color=#000000]'+str(self.pupas_camaron.selected)+'[/color]',markup = True)
		self.p6 = Label(text='[color=#000000]'+str(self.pupas_chich.selected)+'[/color]',markup = True)
		self.p7 = Label(text='[color=#000000]'+str(self.pupas_revuelta.selected)+'[/color]',markup = True)
		self.mm.posR = False
		self.mm.realpos = (535,260)
		self.mm.realsize = (0,0)
		self.p1.posR = False
		self.p1.realpos = (55,235)
		self.p1.realsize = (0,0)
		self.p2.posR = False
		self.p2.realpos = (125,235)
		self.p2.realsize = (0,0)
		self.p3.posR = False
		self.p3.realpos = (195,235)
		self.p3.realsize = (0,0)
		self.p4.posR = False
		self.p4.realpos = (265,235)
		self.p4.realsize = (0,0)
		self.p5.posR = False
		self.p5.realpos = (335,235)
		self.p5.realsize = (0,0)
		self.p6.posR = False
		self.p6.realpos = (405,235)
		self.p6.realsize = (0,0)
		self.p7.posR = False
		self.p7.realpos = (475,235)
		self.p7.realsize = (0,0)
		self.add_widget(self.mm)
		self.add_widget(self.p1)
		self.add_widget(self.p2)
		self.add_widget(self.p3)
		self.add_widget(self.p4)
		self.add_widget(self.p5)
		self.add_widget(self.p6)
		self.add_widget(self.p7)

	def RefreshBar(self):
		self.mm.text='[color=#000000]'+str(self.MONEY)+'[/color]'
		self.p1.text='[color=#000000]'+str(self.pupas_queso.selected)+'[/color]'
		self.p2.text='[color=#000000]'+str(self.pupas_frijol.selected)+'[/color]'
		self.p3.text='[color=#000000]'+str(self.pupas_fq.selected)+'[/color]'
		self.p4.text='[color=#000000]'+str(self.pupas_loroco.selected)+'[/color]'
		self.p5.text='[color=#000000]'+str(self.pupas_camaron.selected)+'[/color]'
		self.p6.text='[color=#000000]'+str(self.pupas_chich.selected)+'[/color]'
		self.p7.text='[color=#000000]'+str(self.pupas_revuelta.selected)+'[/color]'
		self.pp1.text='[color=#000000]'+str(self.pupas_queso.ammount)+'[/color]'
		self.pp2.text='[color=#000000]'+str(self.pupas_frijol.ammount)+'[/color]'
		self.pp3.text='[color=#000000]'+str(self.pupas_fq.ammount)+'[/color]'
		self.pp4.text='[color=#000000]'+str(self.pupas_loroco.ammount)+'[/color]'
		self.pp5.text='[color=#000000]'+str(self.pupas_camaron.ammount)+'[/color]'
		self.pp6.text='[color=#000000]'+str(self.pupas_chich.ammount)+'[/color]'
		self.pp7.text='[color=#000000]'+str(self.pupas_revuelta.ammount)+'[/color]'

	def MoveClient(self,c=None,type=0):
		if c:
			cy = c.pos[1]
			if type == 0:
				self.anim = Animation(x=self.width,duration=5)
				self.anim &= Animation(y=cy-10,duration=1.33,t='in_out_bounce')+Animation(y=cy,duration=1.60,t='in_out_bounce')+Animation(y=cy-10,duration=1.60,t='in_out_bounce')
				self.anim.start(c)

		

	def AddP2I(self,p):
		self.PUP_INV.append(p)
		t = p.getType()
		if t == 'pq':
			self.pupas_queso.ammount = self.pupas_queso.ammount + 1
		elif t == 'pf':
			self.pupas_frijol.ammount = self.pupas_frijol.ammount + 1
		elif t == 'pfq':
			self.pupas_fq.ammount = self.pupas_fq.ammount + 1
		elif t == 'pl':
			self.pupas_loroco.ammount = self.pupas_loroco.ammount + 1
		elif t == 'pc':
			self.pupas_camaron.ammount = self.pupas_camaron.ammount + 1
		elif t == 'pch':
			self.pupas_chich.ammount = self.pupas_chich.ammount + 1
		elif t == 'pr':
			self.pupas_revuelta.ammount = self.pupas_revuelta.ammount + 1

		self.RefreshBar()

		if DEBUG:
			self.redraw()

	def redraw(self):
		self.dbg.l1.text='[color=#FFCC00]'+str(len(self.PUP_INV))+'[/color]'
		if self.selected_client:
			self.dbg.l2.text='[color=#FF0000]'+str(self.selected_client[0])+'[/color]'
		self.dbg.l3.text='[color=#33CC33]'+str(self.MONEY)+'[/color]'

	def HideHands(self):
		self.hands.handsDisappear()
		self.hand_cntl.img = 'handcntrl1.png'
		self.hands.img = 'hands1.png'
		self.hand_cntl.onleft = True
		self.showingh = False
		self.remove_widget(self.hand_cntl)

	def ShowHands(self):
		if not self.showingh:
			self.showingh = True
			self.hand_cntl.realpos = (18,-225)
			self.hand_cntl.size = (70,70)
			self.add_widget(self.hand_cntl)
			self.hands.handsAppear()
		

	def on_touch_move(self, touch):
		if not(PAUSE):
			if not(self.screenup):
				if not(self.slide):
					if touch.dy > 7:
						if self.slot1.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(1) != None:
								self.slot1.sendup()
								p = self.DEFAULT_COMAL.checkOn(1)
								self.DEFAULT_COMAL.remove(1)
								self.AddP2I(p)
						if self.slot2.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(2) != None:
								self.slot2.sendup()
								p = self.DEFAULT_COMAL.checkOn(2)
								self.DEFAULT_COMAL.remove(2)
								self.AddP2I(p)
						if self.slot3.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(3) != None:
								self.slot3.sendup()
								p = self.DEFAULT_COMAL.checkOn(3)
								self.DEFAULT_COMAL.remove(3)
								self.AddP2I(p)
						if self.slot4.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(4) != None:
								self.slot4.sendup()
								p = self.DEFAULT_COMAL.checkOn(4)
								self.DEFAULT_COMAL.remove(4)
								self.AddP2I(p)
						if self.slot5.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(5) != None:
								self.slot5.sendup()
								p = self.DEFAULT_COMAL.checkOn(5)
								self.DEFAULT_COMAL.remove(5)
								self.AddP2I(p)
						if self.slot6.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(6) != None:
								self.slot6.sendup()
								p = self.DEFAULT_COMAL.checkOn(6)
								self.DEFAULT_COMAL.remove(6)
								self.AddP2I(p)
						if self.slot7.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(7) != None:
								self.slot7.sendup()
								p = self.DEFAULT_COMAL.checkOn(7)
								self.DEFAULT_COMAL.remove(7)
								self.AddP2I(p)
						if self.slot8.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(8) != None:
								self.slot8.sendup()
								p = self.DEFAULT_COMAL.checkOn(8)
								self.DEFAULT_COMAL.remove(8)
								self.AddP2I(p)
						if self.slot9.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(9) != None:
								self.slot9.sendup()
								p = self.DEFAULT_COMAL.checkOn(9)
								self.DEFAULT_COMAL.remove(9)
								self.AddP2I(p)
						if self.slot10.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(10) != None:
								self.slot10.sendup()
								p = self.DEFAULT_COMAL.checkOn(10)
								self.DEFAULT_COMAL.remove(10)
								self.AddP2I(p)
						if self.slot11.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(11) != None:
								self.slot11.sendup()
								p = self.DEFAULT_COMAL.checkOn(11)
								self.DEFAULT_COMAL.remove(11)
								self.AddP2I(p)
						if self.slot12.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(12) != None:
								self.slot12.sendup()
								p = self.DEFAULT_COMAL.checkOn(12)
								self.DEFAULT_COMAL.remove(12)
								self.AddP2I(p)
						if self.slot13.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(13) != None:
								self.slot13.sendup()
								p = self.DEFAULT_COMAL.checkOn(13)
								self.DEFAULT_COMAL.remove(13)
								self.AddP2I(p)
						if self.slot14.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(14) != None:
								self.slot14.sendup()
								p = self.DEFAULT_COMAL.checkOn(14)
								self.DEFAULT_COMAL.remove(14)
								self.AddP2I(p)
						if self.slot15.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(15) != None:
								self.slot15.sendup()
								p = self.DEFAULT_COMAL.checkOn(15)
								self.DEFAULT_COMAL.remove(15)
								self.AddP2I(p)
						if self.slot16.collide_point(touch.x,touch.y):
							if self.DEFAULT_COMAL.checkOn(16) != None:
								self.slot16.sendup()
								p = self.DEFAULT_COMAL.checkOn(16)
								self.DEFAULT_COMAL.remove(16)
								self.AddP2I(p)


			if touch.dy > 30:
				if self.screenup:
					anim = Animation(y=+self.height)
					anim.bind(on_complete=self.finished_sliding)
					self.slide = True
					anim.start(self)
					self.screenup = False
					if self.clientarray.c1:
						self.clientarray.c1.ClearBubble()


				
			elif touch.dy < -30:
				anim = Animation(y=0)
				anim.bind(on_complete=self.finished_sliding)
				self.slide = True
				anim.start(self)
				self.screenup = True
				self.HideHands()

	def on_touch_down(self, touch):
		if not(PAUSE):
			if not(self.slide):
				if self.pupas_queso.collide_point(touch.x,touch.y):
					if self.pupas_queso.ammount > self.pupas_queso.selected:
						self.pupas_queso.selected = self.pupas_queso.selected +1
						self.RefreshBar()

				elif self.pupas_frijol.collide_point(touch.x,touch.y):
					if self.pupas_frijol.ammount > self.pupas_frijol.selected:
						self.pupas_frijol.selected = self.pupas_frijol.selected +1
						self.RefreshBar()

				elif self.pupas_fq.collide_point(touch.x,touch.y):
					if self.pupas_fq.ammount > self.pupas_fq.selected:
						self.pupas_fq.selected = self.pupas_fq.selected +1
						self.RefreshBar()

				elif self.pupas_loroco.collide_point(touch.x,touch.y):
					if self.pupas_loroco.ammount > self.pupas_loroco.selected:
						self.pupas_loroco.selected = self.pupas_loroco.selected +1
						self.RefreshBar()
				elif self.pupas_camaron.collide_point(touch.x,touch.y):
					if self.pupas_camaron.ammount > self.pupas_camaron.selected:
						self.pupas_camaron.selected = self.pupas_camaron.selected +1
						self.RefreshBar()

				elif self.pupas_revuelta.collide_point(touch.x,touch.y):
					if self.pupas_revuelta.ammount > self.pupas_revuelta.selected:
						self.pupas_revuelta.selected = self.pupas_revuelta.selected +1
						self.RefreshBar()

				elif self.pupas_chich.collide_point(touch.x,touch.y):
					if self.pupas_chich.ammount > self.pupas_chich.selected:
						self.pupas_chich.selected = self.pupas_chich.selected +1
						self.RefreshBar()

				if self.masa.collide_point(touch.x,touch.y):
					if not(self.showingh):
						self.ShowHands()
						self.p = Pupusa()

				if self.accept.collide_point(touch.x,touch.y):
					###SELLING PUPAS ###
					sold = []
					if self.selected_client:
						for tipo in self.selected_client[0].pedido:
							if tipo == 'queso':
								if self.pupas_queso.selected > 0:
									ls = self.returnPupasof('pq')
									if self.pupas_queso.selected <= len(ls):
										left = ls[self.pupas_queso.selected:]
										sold = ls[:self.pupas_queso.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pq')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'frijol':
								if self.pupas_frijol.selected > 0:
									ls = self.returnPupasof('pf')
									if self.pupas_frijol.selected <= len(ls):
										left = ls[self.pupas_frijol.selected:]
										sold = ls[:self.pupas_frijol.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pf')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'frijol-queso':
								if self.pupas_fq.selected > 0:
									ls = self.returnPupasof('pfq')
									if self.pupas_fq.selected <= len(ls):
										left = ls[self.pupas_fq.selected:]
										sold = ls[:self.pupas_fq.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pfq')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'loroco':
								if self.pupas_loroco.selected > 0:
									ls = self.returnPupasof('pl')
									if self.pupas_loroco.selected <= len(ls):
										left = ls[self.pupas_loroco.selected:]
										sold = ls[:self.pupas_loroco.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pl')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'camaron':
								if self.pupas_camaron.selected > 0:
									ls = self.returnPupasof('pc')
									if self.pupas_camaron.selected <= len(ls):
										left = ls[self.pupas_camaron.selected:]
										sold = ls[:self.pupas_camaron.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pc')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'chich':
								if self.pupas_chich.selected > 0:
									ls = self.returnPupasof('pch')
									if self.pupas_chich.selected <= len(ls):
										left = ls[self.pupas_chich.selected:]
										sold = ls[:self.pupas_chich.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pch')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
							elif tipo == 'revuelta':
								if self.pupas_revuelta.selected > 0:
									ls = self.returnPupasof('pr')
									if self.pupas_revuelta.selected <= len(ls):
										left = ls[self.pupas_revuelta.selected:]
										sold = ls[:self.pupas_revuelta.selected]
										for s in sold:
											self.MONEY = self.MONEY + s.getPrice()
									self.PUP_INV = self.returnAllbut('pr')
									self.PUP_INV.extend(left)
									self.UpdateAmmounts()
						if sold:
							self.clientarray.EnterClient(self.selected_client[1],0)
						if DEBUG:
							self.redraw()
						try:
							self.clientarray.c1.ClearBubble()
						except:
							pass
						self.ClearSelected()
							#cant = self.selected_client.pedido[tipo]


				elif self.cancel.collide_point(touch.x,touch.y):
					self.ClearSelected()

				if self.clientarray.c1:
					if self.clientarray.c1.collide_point(touch.x,touch.y):
						print self.clientarray.c1.pos
						if self.clientarray.c1.notmoving:
							self.clientarray.c1.ShowPedido()
				if self.clientarray.c2:
					if self.clientarray.c2.collide_point(touch.x,touch.y):
						print self.clientarray.c2.pos
						if self.clientarray.c2.notmoving:
							self.clientarray.c2.ShowPedido()
				if self.clientarray.c3:
					if self.clientarray.c3.collide_point(touch.x,touch.y):
						print self.clientarray.c3.pos
						if self.clientarray.c3.notmoving:
							self.clientarray.c3.ShowPedido()
				if self.clientarray.c4:
					if self.clientarray.c4.collide_point(touch.x,touch.y):
						print self.clientarray.c4.pos
						if self.clientarray.c4.notmoving:
							self.clientarray.c4.ShowPedido()

				if self.showingh:
					if self.p != None:
						if self.queso.collide_point(touch.x,touch.y):
							if self.p.has('queso'):
								self.hands.shake()
							else:
								self.p.AddIngredient('queso')
						elif self.frijol.collide_point(touch.x,touch.y):
							if self.p.has('frijol'):
								self.hands.shake()
							else:
								self.p.AddIngredient('frijol')
						elif self.camaron.collide_point(touch.x,touch.y):
							if self.p.has('camaron'):
								self.hands.shake()
							else:
								self.p.AddIngredient('camaron')
						elif self.chich.collide_point(touch.x,touch.y):
							if self.p.has('chich'):
								self.hands.shake()
							else:
								self.p.AddIngredient('chich')
						elif self.loroco.collide_point(touch.x,touch.y):
							if self.p.has('loroco'):
								self.hands.shake()
							else:
								self.p.AddIngredient('loroco')
						elif self.garbage.collide_point(touch.x,touch.y):
							self.HideHands()
							self.p = None
						elif self.hand_cntl.collide_point(touch.x,touch.y):
							if self.hand_cntl.onleft:
								self.p.Tap()
								self.hand_cntl.realpos = (519,-225)
								self.hand_cntl.onleft = False
								self.hands.img = 'hands2.png'
								self.hand_cntl.img = 'handcntrl2.png'
							else:
								self.p.Tap()
								self.hand_cntl.realpos = (18,-225)
								self.hand_cntl.onleft = True
								self.hand_cntl.img = 'handcntrl1.png'
								self.hands.img = 'hands1.png'
							self.reposition_children(self.children)

				if self.slot1.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(1):
						self.DEFAULT_COMAL.checkOn(1).Flip()
					elif self.p:
						self.addToSlotn(1)
				elif self.slot2.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(2):
						self.DEFAULT_COMAL.checkOn(2).Flip()
					elif self.p:
						self.addToSlotn(2)
				elif self.slot3.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(3):
						self.DEFAULT_COMAL.checkOn(3).Flip()
					elif self.p:
						self.addToSlotn(3)
				elif self.slot4.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(4):
						self.DEFAULT_COMAL.checkOn(4).Flip()
					elif self.p:
						self.addToSlotn(4)
				elif self.slot5.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(5):
						self.DEFAULT_COMAL.checkOn(5).Flip()
					elif self.p:
						self.addToSlotn(5)
				elif self.slot6.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(6):
						self.DEFAULT_COMAL.checkOn(6).Flip()
					elif self.p:
						self.addToSlotn(6)
				elif self.slot7.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(7):
						self.DEFAULT_COMAL.checkOn(7).Flip()
					elif self.p:
						self.addToSlotn(7)
				elif self.slot8.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(8):
						self.DEFAULT_COMAL.checkOn(8).Flip()
					elif self.p:
						self.addToSlotn(8)
				elif self.slot9.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(9):
						self.DEFAULT_COMAL.checkOn(9).Flip()
					elif self.p:
						self.addToSlotn(9)
				elif self.slot10.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(10):
						self.DEFAULT_COMAL.checkOn(10).Flip()
					elif self.p:
						self.addToSlotn(10)
				elif self.slot11.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(11):
						self.DEFAULT_COMAL.checkOn(11).Flip()
					elif self.p:
						self.addToSlotn(11)
				elif self.slot12.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(12):
						self.DEFAULT_COMAL.checkOn(12).Flip()
					elif self.p:
						self.addToSlotn(12)
				elif self.slot13.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(13):
						self.DEFAULT_COMAL.checkOn(13).Flip()
					elif self.p:
						self.addToSlotn(13)
				elif self.slot14.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(14):
						self.DEFAULT_COMAL.checkOn(14).Flip()
					elif self.p:
						self.addToSlotn(14)
				elif self.slot15.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(15):
						self.DEFAULT_COMAL.checkOn(15).Flip()
					elif self.p:
						self.addToSlotn(15)
				elif self.slot16.collide_point(touch.x,touch.y):
					if self.DEFAULT_COMAL.checkOn(16):
						self.DEFAULT_COMAL.checkOn(16).Flip()
					elif self.p:
						self.addToSlotn(16)

	def addToSlotn(self,n=1):
		if self.p.getIngredients():
			self.DEFAULT_COMAL.AddToComal(self.p,n)
			self.p = None
			self.HideHands()


class DebugWindow(ChildBoxLayout):
	
	def __init__(self,**kwargs):
		super(DebugWindow, self).__init__(**kwargs)
		self.l1 = Label(text='',markup = True)
		self.l2 = Label(text='',markup = True)
		self.l3 = Label(text='',markup = True)
		self.add_widget(self.l1)
		self.add_widget(self.l2)
		self.add_widget(self.l3)

class PupusaSlot(ChildWidget):
	img = StringProperty('empty.png')
	def __init__(self,**kwargs):
		super(PupusaSlot, self).__init__(**kwargs)
		self.realsize = (45,40)

	def sendup(self):
		anim = Animation(y=self.parent.height*2,x=self.parent.center_x,duration=0.25)
		anim.bind(on_complete=self.animation_complete)
		anim.start(self)

	def animation_complete(self,a,b):
		self.parent.reposition_children([self])
		self.img = 'empty.png'

class Ingredient(ChildWidget):
	img = StringProperty('gm1.png')
	def __init__(self,ing='masa',cant=100, **kwargs):
		super(Ingredient, self).__init__(**kwargs)
		self.cant = cant
		self.ing = ing
		self.realsize = (75,50)
			

class Timer(ChildWidget):

	def __init__(self, **kwargs):
		super(Timer, self).__init__(**kwargs)
		self.secs = 0
	
	def Start(self):
		Clock.schedule_interval(self.PlanchaON, TICK)
		Clock.schedule_interval(self.Skychange, 3)
		Clock.schedule_interval(self.GenerateClient, 1)
		#Clock.schedule_interval(self.GenerateClient, int(TICK*77))
		Clock.schedule_once(self.PlayRooster,3)
		Clock.schedule_once(self.PlayChucho,GAME_LEN-5)

	def PlayChucho(self, dt):
		playsound(SOUND_FILES['chucho'])
		pass

	def PlayRooster(self, dt):
		playsound(SOUND_FILES['gallo'])
		pass
		
	def Stop(self):
		Clock.unschedule(self.PlanchaON)
		Clock.unschedule(self.GenerateClient)
		self.parent.parent.show_buymaterials(self.parent.MONEY,0,0,0,0)

	def GenerateClient(self, dt):
		if getrandbits(1):
			if not(self.parent.clientarray.c1):
				cl1 = ClientWidget(Cliente(self.parent.ladrones,self.parent.Prisa,self.parent.max_pup,self.parent.max_var,self.parent.lim))
				self.parent.clientarray.c1 = cl1
				cl1.pos = (-200,0)
				cl1.realpos = (-200,0)
				self.parent.clientarray.EnterClient(cl1,1)
				self.parent.clientarray.add_widget(cl1)
			elif not(self.parent.clientarray.c2):
				cl2 = ClientWidget(Cliente(self.parent.ladrones,self.parent.Prisa,self.parent.max_pup,self.parent.max_var,self.parent.lim))
				self.parent.clientarray.c2 = cl2
				cl2.pos = (-200,0)
				cl2.realpos = (-200,0)
				self.parent.clientarray.EnterClient(cl2,2)
				self.parent.clientarray.add_widget(cl2)
			elif not(self.parent.clientarray.c3):
				cl3 = ClientWidget(Cliente(self.parent.ladrones,self.parent.Prisa,self.parent.max_pup,self.parent.max_var,self.parent.lim))
				self.parent.clientarray.c3 = cl3
				cl3.pos = (-200,0)
				cl3.realpos = (-200,0)
				self.parent.clientarray.EnterClient(cl3,3)
				self.parent.clientarray.add_widget(cl3)
			elif not(self.parent.clientarray.c4):
				cl4 = ClientWidget(Cliente(self.parent.ladrones,self.parent.Prisa,self.parent.max_pup,self.parent.max_var,self.parent.lim))
				self.parent.clientarray.c4 = cl4
				cl4.pos = (-200,0)
				cl4.realpos = (-200,0)
				self.parent.clientarray.EnterClient(cl4,4)
				self.parent.clientarray.add_widget(cl4)

			if DEBUG:
				self.parent.redraw()

	def Skychange(self, dt):
		if self.parent.skyn < len(SKY_COLORS) - 1:
			self.parent.skyn = self.parent.skyn + 1
		self.parent.r = SKY_COLORS[self.parent.skyn][0]
		self.parent.g = SKY_COLORS[self.parent.skyn][1]
		self.parent.b = SKY_COLORS[self.parent.skyn][2]

	def PlanchaON(self, dt):
		self.secs = self.secs + 1
		self.parent.DEFAULT_COMAL.ON()
		if self.parent.DEFAULT_COMAL.checkOn(1):
			p = self.parent.DEFAULT_COMAL.checkOn(1)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot1.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(2):
			p = self.parent.DEFAULT_COMAL.checkOn(2)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot2.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(3):
			p = self.parent.DEFAULT_COMAL.checkOn(3)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot3.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(4):
			p = self.parent.DEFAULT_COMAL.checkOn(4)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot4.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(5):
			p = self.parent.DEFAULT_COMAL.checkOn(5)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot5.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(6):
			p = self.parent.DEFAULT_COMAL.checkOn(6)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot6.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(7):
			p = self.parent.DEFAULT_COMAL.checkOn(7)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot7.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(8):
			p = self.parent.DEFAULT_COMAL.checkOn(8)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot8.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(9):
			p = self.parent.DEFAULT_COMAL.checkOn(9)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot9.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(10):
			p = self.parent.DEFAULT_COMAL.checkOn(10)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot10.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(11):
			p = self.parent.DEFAULT_COMAL.checkOn(11)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot11.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(12):
			p = self.parent.DEFAULT_COMAL.checkOn(12)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot12.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(13):
			p = self.parent.DEFAULT_COMAL.checkOn(13)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot13.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(14):
			p = self.parent.DEFAULT_COMAL.checkOn(14)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot14.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(15):
			p = self.parent.DEFAULT_COMAL.checkOn(15)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot15.img = t+t2
		if self.parent.DEFAULT_COMAL.checkOn(16):
			p = self.parent.DEFAULT_COMAL.checkOn(16)
			if p.taps < 10:
				t = 'pm'
			elif p.taps > 10:
				t = 'pm'
			else:
				t = p.getType()
			t2 = p.getImg()
			self.parent.slot16.img = t+t2
		if self.secs > (GAME_LEN/float(TICK)):
			print (GAME_LEN/float(TICK))
			print self.parent.DEFAULT_COMAL
			self.Stop()

class PedidoPupa(AnchorLayout):
	img = StringProperty('pc1.png')
	imagedir = StringProperty(path.join(getcwd()+'/images/'))
	realpos = (90,250)
	posR = BooleanProperty(False)
	realsize = (100,100)
	def __init__(self,t='pc1.png',n=1,pos=[0,0], **kwargs):
		super(PedidoPupa, self).__init__(**kwargs)
		self.img = t
		self.n = n

	def reposition_children(self, children):
		for child in children: #dialog
			child.pos = self.pos[0],self.pos[1]-(self.size[1]*0.5)


class PedidoLabel(Label):
	realpos = (0,0)
	realsize = (0,0)


class PedidoBubble(BoxLayout):
	imagedir = StringProperty(path.join(getcwd()+'/images/'))
	img = StringProperty('globo.png')
	realpos = (90,250)
	posR = BooleanProperty(False)
	realsize = (100,100)

	def __init__(self,pedido=None,ppos=[0,0], **kwargs):
		super(PedidoBubble, self).__init__(**kwargs)
		self.pedido = pedido
		self.ppos = ppos

	def GeneratePupa(self,already=[]):
		if 'queso' in self.pedido and (not ('queso'in already)):
			p1 = PedidoPupa('pq1.png',self.pedido['queso'],self.ppos)
		elif 'frijol' in self.pedido and (not ('frijol'in already)):
			p1 = PedidoPupa('pf1.png',self.pedido['frijol'],self.ppos)
		elif 'frijol-queso' in self.pedido and (not ('frijol-queso'in already)):
			p1 = PedidoPupa('pfq1.png',self.pedido['frijol-queso'],self.ppos)
		elif 'loroco' in self.pedido and (not ('loroco'in already)):
			p1 = PedidoPupa('pl1.png',self.pedido['loroco'],self.ppos)
		elif 'camaron' in self.pedido and (not ('camaron'in already)):
			p1 = PedidoPupa('pc1.png',self.pedido['camaron'],self.ppos)
		elif 'chich' in self.pedido and (not ('chich'in already)):
			p1 = PedidoPupa('pch1.png',self.pedido['chich'],self.ppos)
		elif 'revuelta' in self.pedido and (not ('revuelta'in already)):
			p1 = PedidoPupa('pr1.png',self.pedido['revuelta'],self.ppos)
		return p1
	
	def DrawPedido(self):
		self.clear_widgets()
		if len(self.pedido) == 1:
			p1 = self.GeneratePupa()
			n = PedidoLabel(text='[color=#000000]'+str(p1.n)+'[/color]',markup = True)
			p1.add_widget(n)
			self.add_widget(p1)

		elif len(self.pedido) == 2:
			p1 = self.GeneratePupa()
			n = PedidoLabel(text='[color=#000000]'+str(p1.n)+'[/color]',markup = True)
			p1.add_widget(n)
			self.add_widget(p1)
			if (p1.img == 'pq1.png'):	already = 'queso'
			if (p1.img == 'pf1.png'):	already = 'frijol'
			if (p1.img == 'pfq1.png'):	already = 'frijol-queso'
			if (p1.img == 'pl1.png'):	already = 'loroco'
			if (p1.img == 'pc1.png'):	already = 'camaron'
			if (p1.img == 'pch1.png'):	already = 'chich'
			if (p1.img == 'pr1.png'):	already = 'revuelta'
			p2 = self.GeneratePupa([already])
			n2 = PedidoLabel(text='[color=#000000]'+str(p2.n)+'[/color]',markup = True)
			p2.add_widget(n2)
			self.add_widget(p2)
		elif len(self.pedido) == 3:
			p1 = self.GeneratePupa()
			n = PedidoLabel(text='[color=#000000]'+str(p1.n)+'[/color]',markup = True)
			p1.add_widget(n)
			self.add_widget(p1)
			if (p1.img == 'pq1.png'):	already = ['queso']
			elif (p1.img == 'pf1.png'):	already = ['frijol']
			elif (p1.img == 'pfq1.png'):	already = ['frijol-queso']
			elif (p1.img == 'pl1.png'):	already = ['loroco']
			elif (p1.img == 'pc1.png'):	already = ['camaron']
			elif (p1.img == 'pch1.png'):	already = ['chich']
			elif (p1.img == 'pr1.png'):	already = ['revuelta']
			p2 = self.GeneratePupa(already)
			n2 = PedidoLabel(text='[color=#000000]'+str(p2.n)+'[/color]',markup = True)
			p2.add_widget(n2)
			self.add_widget(p2)
			if (p2.img == 'pq1.png'):	already.append('queso')
			elif (p2.img == 'pf1.png'):	already.append('frijol')
			elif (p2.img == 'pfq1.png'):	already.append('frijol-queso')
			elif (p2.img == 'pl1.png'):	already.append('loroco')
			elif (p2.img == 'pc1.png'):	already.append('camaron')
			elif (p2.img == 'pch1.png'):	already.append('chich')
			elif (p2.img == 'pr1.png'):	already.append('revuelta')
			p3 = self.GeneratePupa(already)
			n3 = PedidoLabel(text='[color=#000000]'+str(p3.n)+'[/color]',markup = True)
			p3.add_widget(n3)
			self.add_widget(p3)



	def reposition_children(self, children):

		for child in children: #dialog
			child.pos = self.size[0]*(child.realpos[0]/600.),self.size[1]*(child.realpos[1]/300.)
					

				

class ClientWidget(ChildWidget):
	img = StringProperty('h1a.png')
	realsize = (125,300)

	def __init__(self,c=None, **kwargs):
		super(ClientWidget, self).__init__(**kwargs)
		self.c = c
		if c:
			self.img = c.img
		self.notmoving = False

	def AnimateBubble(self):
		self.anim = Animation(y = self.buble.pos[1]+20,duration=0.3) + Animation(y = self.buble.pos[1],duration=0.3)
		self.anim.bind(on_complete=self.ClearBubble)
		self.anim.start(self.buble)

	def ClearBubble(self,a=None,b=None):
		for c in self.parent.children:
			c.clear_widgets()
		

	def ShowPedido(self):
		self.ClearBubble()
		self.parent.parent.selected_client = (self.c,self)
		if DEBUG:
			self.parent.parent.redraw()
		self.buble = PedidoBubble(orientation='horizontal',size_hint=(0.5,0.5),pedido=self.c.pedido,ppos=(self.pos[0]+self.size[0]*0.5,self.size[1]*0.75))
		self.buble.padding = [5,5,5,15]
		self.add_widget(self.buble)
		self.buble.DrawPedido()
		self.reposition_children(self.children)
		self.AnimateBubble()

	def reposition_children(self, children):
		for child in children:
			if child.posR:
				child.pos = self.size[0]*(child.realpos[0]/150.),self.size[1]*(child.realpos[1]/300.)
			else:
				child.pos = self.size[0]*(child.realpos[0]/150.) + self.pos[0],self.size[1]*(child.realpos[1]/300.) + self.pos[1]
			child.size_hint = (self.size[0]/150.,self.size[1]/300.)
			child.size = (child.size_hint[0]*child.realsize[0],child.size_hint[1]*child.realsize[1])






class Garbage(ChildWidget):
	img = StringProperty('gbg1.png')


class Hands(ChildWidget):
	img = StringProperty('hands1.png')
	ismov = BooleanProperty(False)
	def shake(self):
		if not(self.ismov):
			temp = self.pos[0]
			anim = Animation(x=temp-50, duration=.1)+Animation(x=temp+50, duration=.1)+Animation(x=temp, duration=.1)
			anim.start(self)
			anim.bind(on_progress=self.moving,on_complete=self.animation_complete)

	def moving(self,a,b,c):
		self.ismov = True

	def handsAppear(self):
		anim = Animation(y=-self.size[1]*0.15, duration=.25)
		anim.start(self)
		anim.bind(on_complete=self.handsup)
		
		
	def handsDisappear(self):
		anim = Animation(y=-self.size[1], duration=0.25)
		anim.start(self)
		self.realpos = (240,-600)

	def animation_complete(self,a,b):
		self.ismov = False

	def handsup(self,a,b):
		self.realpos = (240,-324)



class HandCntrl(ChildWidget):
	onleft = BooleanProperty(True)
	img = StringProperty('handcntrl1.png')

class GameLogo(ChildWidget):
	img = StringProperty('logo.png')


class TransitionScreen(ScreenWidget):
	img = StringProperty('titlebg.png')
	def __init__(self, **kwargs):
		super(TransitionScreen, self).__init__(**kwargs)
		with self.canvas:
			Rectangle(color=(1,1,1),pos=(self.width,0.5))

class TitleScreen(ScreenWidget):
	img = StringProperty('titlebg.png')
	moving = BooleanProperty(False)
	def __init__(self, **kwargs):
		super(TitleScreen, self).__init__(**kwargs)
		self.logo = GameLogo()
		self.add_widget(self.logo)
		self.logo.realpos = (30,75)
		self.logo.realsize = (300,200)
		self.logoup = False
		self.bbg = BlackBG()
		self.bbg.realpos = (30,15)
		self.add_widget(self.bbg)
		self.bbg.realsize = (540,270)

	def dontinterrupt(self):
		self.moving = True

	def move_logodown(self):
		if not(self.moving):
			self.anim = Animation(y=self.parent.height/4.,duration=1)
			self.anim.start(self.logo)
			self.anim.bind(on_complete=self.movelogodown_animation_complete)

	def move_logoup(self):
		if not(self.moving):
			self.anim = Animation(y=self.height*2.,duration=1)
			self.anim.start(self.logo)
			self.anim.bind(on_complete=self.movelogo_animation_complete)

	def move_logoup_startgame(self,a):
		if not(self.moving):
			self.anim = Animation(y=self.height*2.,duration=1)
			self.anim.start(self.logo)
			self.anim.bind(on_complete=self.go)

	def go(self,a,b):
		print a,b
		self.parent.show_consejo()

	def fadein(self):
		if not(self.moving):
			self.fade = Animation(a=0.5,duration=1)
			self.fade.start(self.bbg)
			self.fade.bind(on_complete=self.fadein_animation_complete)

	def fadeout(self):
		if not(self.moving):
			self.fade = Animation(a=0,duration=1)
			self.fade.start(self.bbg)
			self.fade.bind(on_complete=self.fadeout_animation_complete)

	def movelogodown_animation_complete(self,obj,obj3):
		self.moving = False
		self.logo.realpos = self.logo.pos


	def movelogo_animation_complete(self,obj,obj3):
		self.moving = False
		self.logo.realpos = self.logo.pos

	def fadeout_animation_complete(self,obj,obj3):
		self.moving = False

	def fadein_animation_complete(self,obj,obj3):
		self.moving = False

	def startgame(self):
		### START SCREEN ###
		self.start_buttonlayout = ChildBoxLayout(orientation='vertical',size_hint=(.5, .7),pos=(self.width*0.6, self.height*0.16))
		self.btn_newgame = Button(text='Comenzar',size_hint=(.7, 1))
		self.btn_highscore = Button(text='Mejor Puntuacion',size_hint=(.7, 1))
		self.btn_credits = Button(text='Creditos',size_hint=(.7, 1))
		self.btn_newgame.bind(on_press=self.move_logoup_startgame)
		self.btn_highscore.bind(on_press=self.highscore)
		self.btn_credits.bind(on_press=self.credits)
		self.start_buttonlayout.add_widget(self.btn_newgame)
		self.start_buttonlayout.add_widget(self.btn_highscore)
		self.start_buttonlayout.add_widget(self.btn_credits)
		self.add_widget(self.start_buttonlayout)
		self.start_buttonlayout.posR = False
		self.start_buttonlayout.pos = (500,30)
		self.start_buttonlayout.realpos = (500,30)


	def highscore(self, a):
		pass

	def credits(self, a):
		self.remove_widget(a.parent)
		self.move_logoup()
		self.fadein()
		self.btn_back = Button(text='Regresar',size_hint=(.50, 0.5))
		self.btn_back.bind(on_progress=self.dontinterrupt,on_press=self.backtomain)
		self.layoutBack = ChildBoxLayout(orientation='horizontal',size_hint=(1, 0.3),pos=(0,35))
		self.layoutBack.add_widget(ChildBoxLayout())
		self.layoutBack.add_widget(self.btn_back)
		self.layoutBack.add_widget(ChildBoxLayout())
		self.parent.add_widget(self.layoutBack)
		

	def backtomain(self,a):
		self.fadeout()
		self.parent.remove_widget(a.parent)
		self.move_logodown()
		self.startgame()

class PupaBar_ConfirmButton(ChildWidget):
	pass

class PupaBar(ChildWidget):
	img = StringProperty('bar.png')
		
class PupaBar_Button(ChildWidget):
	img = StringProperty('pc1.png')
	ammount = NumericProperty(0)
	selected = NumericProperty(0)
	def __init__(self,t=0,a=0, **kwargs):
		super(PupaBar_Button, self).__init__(**kwargs)
		self.t = t
		if t == 0:
			self.img = 'pq1.png'
		elif t == 1:
			self.img = 'pf1.png'
		elif t == 2:
			self.img = 'pfq1.png'
		elif t == 3:
			self.img = 'pl1.png'
		elif t == 4:
			self.img = 'pc1.png'
		elif t == 5:
			self.img = 'pch1.png'
		elif t == 6:
			self.img = 'pr1.png'
		self.ammount = 0


class BlackBG(ChildWidget):
	a = NumericProperty(0.0)

class ArrayOfClients(ChildWidget):
	posR = BooleanProperty(False)
	realsize = (600,300)
	realpos = (0,0)
	def __init__(self, **kwargs):
		super(ArrayOfClients, self).__init__(**kwargs)
		self.c1 = ObjectProperty(None)
		self.c2 = ObjectProperty(None)
		self.c3 = ObjectProperty(None)
		self.c4 = ObjectProperty(None)
		"""
		self.c1.realpos = (200,0)
		self.c2.realpos = (125,0)
		self.c3.realpos = (50,0)
		self.c4.realpos = (-25,0)
		self.ReorderStartingFrom(1)
		"""
	def EnterClient(self,c,position=1):

		if position == 1:
			m = -(self.parent.size[0]/24.)
		elif position == 2:
			m = self.parent.size[0]/12.
		elif position == 3:
			m = self.parent.size[0]/4.8
		elif position == 4:
			m = self.parent.size[0]/3.
		elif position == 0:
			m = self.parent.size[0]
		anim = Animation(x=m,duration=5)
		if position == 0:
			anim.bind(on_complete=self.removeClient,on_progress=self.animation_prog)
		elif position == 1:
			anim.bind(on_complete=self.animation_complete_1,on_progress=self.animation_prog)
		elif position == 2:
			anim.bind(on_complete=self.animation_complete_2,on_progress=self.animation_prog)
		elif position == 3:
			anim.bind(on_complete=self.animation_complete_3,on_progress=self.animation_prog)
		elif position == 4:
			anim.bind(on_complete=self.animation_complete_4,on_progress=self.animation_prog)
		anim.start(c)

	def removeClient(self,a,c):
		c.realpos = 750,0
		c.notmoving = True
		if self.c1 == c:
			self.remove_widget(self.c1)
			self.c1 = None
		elif self.c2 == c:
			self.remove_widget(self.c2)
			self.c2 = None
		elif self.c3 == c:
			self.remove_widget(self.c3)
			self.c3 = None
		elif self.c4 == c:
			self.remove_widget(self.c4)
			self.c4 = None

	def animation_complete_1(self,a,c):
		c.realpos = -25,0
		c.notmoving = True
	def animation_complete_2(self,a,c):
		c.realpos = 50,0
		c.notmoving = True
	def animation_complete_3(self,a,c):
		c.realpos = 125,0
		c.notmoving = True
	def animation_complete_4(self,a,c):
		c.realpos = 200,0
		c.notmoving = True

	def animation_prog(self,a,c,f):
		### THIS HERE NEEDS FIX ###
		### THIS HERE NEEDS FIX ###
		### THIS HERE NEEDS FIX ###
		c.notmoving = False
		### THIS HERE NEEDS FIX ###
		### THIS HERE NEEDS FIX ###
		### THIS HERE NEEDS FIX ###

	def ReorderStartingFrom(self,n=1):
		self.clear_widgets()
		if n == 1:
			self.add_widget(self.c4)
			self.add_widget(self.c3)
			self.add_widget(self.c2)
			self.add_widget(self.c1)
		elif n == 2:
			self.add_widget(self.c1)
			self.add_widget(self.c4)
			self.add_widget(self.c3)
			self.add_widget(self.c2)
		elif n == 3:
			self.add_widget(self.c2)
			self.add_widget(self.c1)
			self.add_widget(self.c4)
			self.add_widget(self.c3)
		elif n == 4:
			self.add_widget(self.c3)
			self.add_widget(self.c2)
			self.add_widget(self.c1)
			self.add_widget(self.c4)
	
	def reposition_children(self, children):
		for child in children:
			child.pos = self.size[0]*(child.realpos[0]/600.),self.size[1]*(child.realpos[1]/300.)+ self.pos[1]
			child.size_hint = (self.size[0]/600.,self.size[1]/300.)
			child.size = (child.size_hint[0]*child.realsize[0],child.size_hint[1]*child.realsize[1])
			child.reposition_children(child.children)
