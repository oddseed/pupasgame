from gamesettings import *
from functions import *
from random import getrandbits,randrange
from sound_layer import *

class Cliente:
	def __init__(self,ladrones=False,Prisa=False,max_pup=5,max_var=1,lim=3):
		if ladrones:
			self.esLadron = not(bool(getrandbits(1)+getrandbits(1)+getrandbits(1)))
		else:
			self.esLadron = False
		if Prisa:
			self.tienePrisa = not(bool(getrandbits(1)+getrandbits(1)+getrandbits(1)))
		else:
			self.tienePrisa = False
		self.pedido = GeneratePedido(max_pup,max_var,lim)
		self.esHombre = bool(getrandbits(1))
		if self.esHombre:
			self.img = IMG_CLIENTES_H[randrange(len(IMG_CLIENTES_H))]
		else:
			self.img = IMG_CLIENTES_M[randrange(len(IMG_CLIENTES_M))]


	def __repr__(self):
		return 'Hombre:'+str(self.esHombre)+' Ladron:'+str(self.esLadron)+' IMG:'+self.img+' \nPedido:'+str(self.pedido)


class Comal:
	espacio = []
	def __init__(self,max_pup=16):
		self.espacio = [None] * max_pup
	
	def __repr__(self):
		return str(self.espacio)

	def ON(self):
		"""
		OCURRE UN ON CADA SEGUNDO
		"""
		for x in self.espacio:
			if x != None:
				x.Cook()
	def sell(self,n=0):
		p = self.espacio[n]
		if p:
			self.espacio[n] = None
			print p.getPrice()
	def checkOn(self,p):
		return self.espacio[p-1]
	def AddToComal(self,p=None,n=1):
		if p != None:
			self.espacio[n-1] = p
	def remove(self,n=1):
		self.espacio[n-1] = None





class Pupusa:
	def __init__(self):
		self.ingredients = {'frijol':False,'queso':False,'chich':False,'camaron':False,'loroco':False}
		self.status = (0,0)
		self.sideA = True
		self.taps = 0

	def __repr__(self):
		return str(self.status) + str(self.getIngredients()) + str(self.taps) + str(self.sideA)

	def show(self):
		print str(self.ingredients), self.status , self.taps

	def getPrice(self):
		a = self.status[0]
		b = self.status[1]
		if a >= 100:
			a = 100-(a-100)
		if b >= 100:
			b = 100-(b-100)
		quality = (a+b)/200.
		if self.taps > REQTAPS:
			self.taps = REQTAPS-(self.taps-REQTAPS)
		tapq = self.taps/float(REQTAPS)
		d ='%.2f' % (DEF_PRICE * quality * tapq)
		return float(d)

	def Tap(self):
		if self.taps < REQTAPS*2:
			self.taps += 1

	def has(self,ing='queso'):
		if ing in self.ingredients:
			if self.ingredients[ing]:
				return True
			else:
				return False
		else:
			return False


	def getIngredients(self):
		ans = []
		for i in self.ingredients:
			if self.ingredients[i]:
				ans.append(i)
		return ans

	def getType(self):
		ans = self.getIngredients()
		if len(ans) == 1:
			if ans[0] == 'queso':
				return 'pq'
			elif ans[0] == 'camaron':
				return 'pc'
			elif ans[0] == 'frijol':
				return 'pf'
			elif ans[0] == 'chich':
				return 'pch'
			elif ans[0] == 'loroco':
				return 'pl'
		elif len(ans) == 2:
			if ((ans[0] == 'queso' or ans[0] == 'loroco') and (ans[1] == 'queso' or ans[1] == 'loroco')):
				return 'pl'
			elif ((ans[0] == 'queso' or ans[0] == 'frijol') and (ans[1] == 'queso' or ans[1] == 'frijol')):
				return 'pfq'
			elif ((ans[0] == 'loroco' or ans[0] == 'frijol') and (ans[1] == 'loroco' or ans[1] == 'frijol')):
				return 'pfq'
			elif ((ans[0] == 'queso' or ans[0] == 'camaron') and (ans[1] == 'queso' or ans[1] == 'camaron')):
				return 'pc'
			elif ((ans[0] == 'camaron') or (ans[1] == 'camaron')):
				return 'pc'
			elif ((ans[0] == 'chich') or (ans[1] == 'chich')):
				return 'pch'
			elif ((ans[0] == 'chich') or (ans[1] == 'chich')):
				return 'pch'
		elif len(ans) == 3:
			if ((ans[0] == 'queso' or ans[0] == 'frijol' or ans[0] == 'chich') and (ans[1] == 'queso' or ans[1] == 'frijol'or ans[1] == 'chich') and (ans[2] == 'queso' or ans[2] == 'frijol'or ans[2] == 'chich')):
				return 'pr'
			elif ((ans[0] == 'queso' or ans[0] == 'frijol' or ans[0] == 'loroco') and (ans[1] == 'queso' or ans[1] == 'frijol'or ans[1] == 'loroco') and (ans[2] == 'queso' or ans[2] == 'frijol'or ans[2] == 'loroco')):
				return 'pfq'
			if ((ans[0] == 'loroco' or ans[0] == 'queso' or ans[0] == 'chich') and (ans[1] == 'loroco' or ans[1] == 'queso'or ans[1] == 'chich') and (ans[2] == 'loroco' or ans[2] == 'queso'or ans[2] == 'chich')):
				return 'pch'
			if ((ans[0] == 'loroco' or ans[0] == 'queso' or ans[0] == 'camaron') and (ans[1] == 'loroco' or ans[1] == 'queso'or ans[1] == 'camaron') and (ans[2] == 'loroco' or ans[2] == 'queso'or ans[2] == 'camaron')):
				return 'pc'	
			else:
				return 'pr'
		elif len(ans) > 3:
			return 'pr'

	def getImg(self):
		if self.sideA:
			if self.status[1] < 50:
				return '0.png'
			elif self.status[1] < 100:
				return '1.png'
			elif self.status[1] < 150:
				return '2.png'
			else:
				return '3.png'
		else:
			if self.status[0] < 50:
				return '0.png'
			elif self.status[0] < 100:
				return '1.png'
			elif self.status[0] < 150:
				return '2.png'
			else:
				return '3.png'



	def Flip(self):
		playsound('flip.wav')
		if self.sideA:
			self.sideA = False
		else: self.sideA = True

	def AddIngredient(self,ing=''):
		if (ing == 'queso' or ing == 'frijol' or ing == 'chich' or ing == 'camaron' or ing == 'loroco'):
			self.ingredients[ing] = True

	def Cook(self):
		a = self.status[0]
		b = self.status[1]
		if self.sideA:
			if a<200:
				self.status = (a+1,b)
		else:
			if b<200:
				self.status = (a,b+1)


