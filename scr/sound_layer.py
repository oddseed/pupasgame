from kivy.core.audio import SoundLoader
from os import getcwd,path

def playsound(name=""):
	if name != "":
		sound = SoundLoader.load(path.join(getcwd()+'/sounds/')+name)
		if sound:
		    sound.play()

#playsound(SOUND_FILES['rooster'])