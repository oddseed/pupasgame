from gamescreens import *
from gamescreens import PAUSE
from random import choice

class Tutu(ChildWidget):
	img = StringProperty('t1a.png') 
	imagedir = StringProperty(path.join(getcwd()+'/images/'))
	def __init__(self, **kwargs):
		super(Tutu, self).__init__(**kwargs)



class Tutorial1(ScreenWidget):
	img = StringProperty('tut1bg.png')

	def __init__(self, **kwargs):
		super(ScreenWidget, self).__init__(**kwargs)
		self.cursor = Tutu()
		self.cursor.realpos = (30,150)
		self.cursor.img = 't1a.png'
		self.add_widget(self.cursor)
		self.anim = Animation(y=self.cursor.realpos[1]+10)+Animation(y=self.cursor.realpos[1])
		self.anim.start(self.cursor)
		self.n = -1

	def on_touch_down(self, touch):
		if self.n < 6:
			self.n = 1+self.n
		if self.n == 5:
			self.cursor.img = 't1g.png'
			self.cursor.realpos = (420,140)
			self.reposition_children(self.children)
		elif self.n == 4:
			self.cursor.img = 't1f.png'
			self.cursor.realpos = (355,140)
			self.reposition_children(self.children)
		elif self.n == 3:
			self.cursor.img = 't1e.png'
			self.cursor.realpos = (290,140)
			self.reposition_children(self.children)	
		elif self.n == 2:
			self.cursor.img = 't1d.png'
			self.cursor.realpos = (225,140)
			self.reposition_children(self.children)
		elif self.n == 1:
			self.cursor.img = 't1c.png'
			self.cursor.realpos = (155,140)
			self.reposition_children(self.children)
		elif self.n == 0:
			self.cursor.img = 't1b.png'
			self.cursor.realpos = (85,140)
			self.reposition_children(self.children)
		elif self.n == 6:
			self.parent.show_mainmenu()
	

class TitleSceenNew(ScreenWidget):
	img = StringProperty('titlebg.png')

	def __init__(self, **kwargs):
		super(ScreenWidget, self).__init__(**kwargs)
		self.layoutv = ChildBoxLayout(orientation='horizontal')
		self.social = ChildBoxLayout(orientation='vertical')
		self.layouth = ChildBoxLayout(orientation='vertical')
		self.btn_fb = ImgButton()
		self.btn_fb.img = 'fb.png'
		self.btn_tw = ImgButton()
		self.btn_tw.img = 'tw.png'
		self.layoutv.add_widget(Label())
		self.social.add_widget(Label())
		self.social.add_widget(Label())
		self.social.add_widget(self.btn_fb)
		self.social.add_widget(self.btn_tw)
		self.social.add_widget(Label())
		self.layoutv.add_widget(self.social)
		self.layoutv.add_widget(self.layouth)
		self.layoutv.add_widget(Label())
		self.btn_start = Button(text='Comenzar',size_hint=(.7, 1))
		self.btn_tutorial = Button(text='Tutorial',size_hint=(.7, 1))
		self.btn_credits = Button(text='Creditos',size_hint=(.7, 1))
		self.layouth.add_widget(Label())
		self.layouth.add_widget(Label())
		self.layouth.add_widget(Label())
		self.layouth.add_widget(self.btn_start)
		self.layouth.add_widget(self.btn_tutorial)
		self.layouth.add_widget(self.btn_credits)
		self.layouth.add_widget(Label())
		self.add_widget(self.layoutv)
		self.layoutv.realpos = (0,0)
		self.layoutv.realsize = (600,300)

	def LoadFB(self,a=None):
		import webbrowser
		webbrowser.open('https://www.facebook.com/pupusasgame')

	def LoadTW(self,a=None):
		import webbrowser
		webbrowser.open('https://twitter.com/intent/tweet?text=Hey%20mara%2C%20estoy%20jugando%20%23pupusasgame%20esta%20chivo!%20http%3A%2F%2Fgoo.gl%2Fxvyac0')

class ConsejoScreen(ScreenWidget):
	d = choice(['c1.png','c2.png','c3.png','c4.png'])
	img = StringProperty(d)

class BuyIngredientScreen(ScreenWidget):
	img = StringProperty('hscore.png')
	def __init__(self,money = 0.0,pupusas=1,clientesc=1,clientese=1,dia=1, **kwargs):
		super(BuyIngredientScreen, self).__init__(**kwargs)
		self.title = ChildBoxLayout(orientation="vertical",size_hint=(.7, 0.2))
		self.layout = ChildGridLayout(cols=3,padding=10,spacing=10)
		self.titulo = Label(text='[color=#000000]Fin del dia de trabajo[/color]',font_size='40sp',markup = True)
		self.ganancias = Label(text='[color=#000000]$'+str(money)+'[/color]',font_size='20sp',markup = True)
		self.pupusas = Label(text='[color=#000000]Pupusas hechadas:\n'+str(pupusas)+'[/color]',font_size='20sp',markup = True)
		self.next = Button(text='Regresar',size_hint=(.7, 1))
		self.again = Button(text='Jugar de Nuevo',size_hint=(.7, 1))
		self.title.add_widget(self.titulo)
		self.title.add_widget(self.ganancias)
		self.layout.add_widget(Label())
		self.layout.add_widget(self.next)
		self.layout.add_widget(self.again)
		self.layout.add_widget(Label())

		self.title.add_widget(self.layout)
		self.add_widget(self.title)
		
		self.title.realsize = (600,300)

class GameOverScreen(ScreenWidget):
	img = StringProperty('fondo-consejo.png')

class Credits(ScreenWidget):
	img = StringProperty('credits.png')

	def __init__(self,money = 0.0,pupusas=1,clientesc=1,clientese=1,dia=1, **kwargs):
		super(Credits, self).__init__(**kwargs)
		self.back = ImgButton()
		self.add_widget(self.back)
		self.back.realpos = (50,50)


class AreUSure(ScreenWidget):
	img = StringProperty('confirm.png')
	def __init__(self, **kwargs):
		super(AreUSure, self).__init__(**kwargs)
		self.yes = ButtonAcept()
		self.no = ButtonCancel()
		self.add_widget(self.yes)
		self.add_widget(self.no)
		self.no.realpos = (375,35)
		self.yes.realpos = (125,35)

	def on_touch_down(self, touch):
		if self.yes.collide_point(touch.x,touch.y):
			self.parent.show_mainmenu()
		elif self.no.collide_point(touch.x,touch.y):
			PAUSE = False
			self.parent.remove_widget(self.parent.confirm)

class InterfaceManager(FloatLayout):

	def __init__(self, **kwargs):
		super(InterfaceManager, self).__init__(**kwargs)
		PAUSE = False
		self.show_mainmenu()

	def show_pause(self):
		try:
			if (self.confirm):
				self.remove_widget(self.confirm)
		except:
			pass
		self.PAUSE = True
		self.confirm = AreUSure()
		self.add_widget(self.confirm)

	def show_buymaterials(self,money = 0.0,pupusas=1,clientesc=1,clientese=1,dia=1):
		self.bm = BuyIngredientScreen(money,pupusas,clientesc,clientese,dia)
		self.clear_widgets()
		self.bm.size_hint = (1,1)
		self.bm.next.bind(on_press=self.show_mainmenu)
		self.bm.again.bind(on_press=self.show_consejo)
		self.add_widget(self.bm)

	def show_gamescreen(self,a=None):
		self.bg = GameScreen()
		self.clear_widgets()
		self.bg.size_hint = (1,1)
		self.add_widget(self.bg)
		self.bg.size = (600,300)

	def show_mainmenu(self,a=None):
		self.starts = TitleSceenNew()
		self.clear_widgets()
		self.add_widget(self.starts)
		self.starts.btn_start.bind(on_press=self.show_consejo)
		self.starts.btn_credits.bind(on_press=self.show_credits)
		self.starts.btn_fb.bind(on_press=self.starts.btn_fb.transition)
		self.starts.btn_fb.call = self.starts.LoadFB	
		self.starts.btn_tw.bind(on_press=self.starts.btn_tw.transition)
		self.starts.btn_tw.call = self.starts.LoadTW
		self.starts.btn_tutorial.bind(on_press=self.show_tutorial)
		self.starts.size_hint = (1,1)
		self.starts.size = (600,300)

	def show_tutorial(self,a=None):
		self.starts = Tutorial1()
		self.clear_widgets()
		self.add_widget(self.starts)
	
	def show_tutorial2(self,a=None):
		self.bg = GameScreen()
		self.clear_widgets()
		self.bg.size_hint = (1,1)
		self.add_widget(self.bg)
		self.tu2 = Tutu()
		self.tu2.img = 't2a.png'
		self.bx = ChildBoxLayout()
		self.bx.add_widget(self.tu2)
		self.bx.add_widget(self.tu2)
		self.add_widget(self.bx)
		self.bg.size = (600,300)
		Clock.schedule_interval(self.tutoadv,5)

	def tutoadv(self,a=None):
		pass


	def show_credits(self,a=None):
		self.credits = Credits()
		self.clear_widgets()
		self.add_widget(self.credits)
		self.credits.back.bind(on_press=self.credits.back.transition)
		self.credits.back.call = self.show_mainmenu
		
	def show_consejo(self,a=None):
		Clock.schedule_once(self.show_gamescreen, 5)
		self.consejo = ConsejoScreen()
		self.clear_widgets()
		self.add_widget(self.consejo)

		

class VisualLayerApp(App):
	use_kivy_settings = BooleanProperty(False) 

	def build_config(self, config):
		Config.set('graphics', 'height', 300)
		Config.set('graphics', 'width', 600)


	def build_settings(self,settings):
		jsondata = ''
		settings.add_json_panel('PupusasGame',self.config, data=jsondata)

	def build(self):
		self.win = Window.bind(on_keyboard=self.hook_keyboard)
		self.layout = InterfaceManager()
		self.layout.size = (600,300)
		return self.layout

	def on_pause(self):
		return True

	def on_resume(self):
		return self.layout

	def hook_keyboard(self, window, key, *largs):
		if key == 27: # BACK
			for b in self.layout.children:
				if isinstance( b, GameScreen):
					self.layout.show_pause()
					break
			return True
		elif key in (282, 319): # SETTINGS
			return True


if __name__ == '__main__':
	VisualLayerApp().run()